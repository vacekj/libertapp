import axios from 'axios'
import { env } from './environment'

export const duneClient = axios.create({
  baseURL: 'https://api.dune.com/api/v1',
  headers: {
    'x-dune-api-key': env.duneApiKey,
  },
})
